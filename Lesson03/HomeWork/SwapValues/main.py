import sys
from swapValues import *


class MyWin(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.ui.btnSwapLR.clicked.connect(self.swapLeftRight)
        self.ui.btnSwapUD.clicked.connect(self.swapUpDown)

    def swapLeftRight(self):
        tmp_text = self.ui.btn1.text()
        self.ui.btn1.setText(self.ui.btn2.text())
        self.ui.btn2.setText(tmp_text)

    def swapUpDown(self):
        tmp_text = self.ui.btn2.text()
        self.ui.btn2.setText(self.ui.btn3.text())
        self.ui.btn3.setText(tmp_text)

app = QtWidgets.QApplication(sys.argv)
window = MyWin()
window.show()
app.exec_()