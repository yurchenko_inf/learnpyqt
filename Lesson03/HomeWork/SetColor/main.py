import sys
from setColor import *


class MyWin(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.ui.slRed.valueChanged.connect(self.colorChange)
        self.ui.slGreen.valueChanged.connect(self.colorChange)
        self.ui.slBlue.valueChanged.connect(self.colorChange)

        self.ui.slBlue.setValue()

    def colorChange(self):
        red = self.ui.slRed.value()
        green = self.ui.slGreen.value()
        blue = self.ui.slBlue.value()

        self.ui.rect.setStyleSheet("background-color: rgb({}, {}, {})".format(red, green, blue))
        print(self.ui.slRed.setValue())

app = QtWidgets.QApplication(sys.argv)
window = MyWin()
window.show()
app.exec_()