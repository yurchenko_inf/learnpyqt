# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/booblegum/Projects/PyQT/LearnPyQt/Lesson03/HomeWork/SetColor/setColor.ui'
#
# Created by: PyQt5 UI code generator 5.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(373, 138)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.rect = QtWidgets.QWidget(self.centralwidget)
        self.rect.setGeometry(QtCore.QRect(250, 10, 101, 91))
        self.rect.setAcceptDrops(False)
        self.rect.setAutoFillBackground(False)
        self.rect.setStyleSheet("background: blue")
        self.rect.setObjectName("rect")
        self.layoutWidget = QtWidgets.QWidget(self.centralwidget)
        self.layoutWidget.setGeometry(QtCore.QRect(10, 10, 221, 80))
        self.layoutWidget.setObjectName("layoutWidget")
        self.formLayout = QtWidgets.QFormLayout(self.layoutWidget)
        self.formLayout.setContentsMargins(0, 0, 0, 0)
        self.formLayout.setObjectName("formLayout")
        self.label = QtWidgets.QLabel(self.layoutWidget)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.slRed = QtWidgets.QSlider(self.layoutWidget)
        self.slRed.setMaximum(255)
        self.slRed.setProperty("value", 0)
        self.slRed.setOrientation(QtCore.Qt.Horizontal)
        self.slRed.setObjectName("slRed")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.slRed)
        self.label_2 = QtWidgets.QLabel(self.layoutWidget)
        self.label_2.setObjectName("label_2")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_2)
        self.slGreen = QtWidgets.QSlider(self.layoutWidget)
        self.slGreen.setMaximum(255)
        self.slGreen.setOrientation(QtCore.Qt.Horizontal)
        self.slGreen.setTickPosition(QtWidgets.QSlider.NoTicks)
        self.slGreen.setTickInterval(100)
        self.slGreen.setObjectName("slGreen")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.slGreen)
        self.label_3 = QtWidgets.QLabel(self.layoutWidget)
        self.label_3.setObjectName("label_3")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_3)
        self.slBlue = QtWidgets.QSlider(self.layoutWidget)
        self.slBlue.setMaximum(255)
        self.slBlue.setProperty("value", 255)
        self.slBlue.setSliderPosition(255)
        self.slBlue.setOrientation(QtCore.Qt.Horizontal)
        self.slBlue.setTickPosition(QtWidgets.QSlider.NoTicks)
        self.slBlue.setObjectName("slBlue")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.slBlue)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Colors"))
        self.label.setText(_translate("MainWindow", "R:"))
        self.label_2.setText(_translate("MainWindow", "G:"))
        self.label_3.setText(_translate("MainWindow", "B:"))


