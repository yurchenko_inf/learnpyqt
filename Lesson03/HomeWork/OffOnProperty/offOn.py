# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/booblegum/Projects/PyQT/LearnPyQt/Lesson03/HomeWork/OffOnProperty/offOn.ui'
#
# Created by: PyQt5 UI code generator 5.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(230, 195)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.widget = QtWidgets.QWidget(self.centralwidget)
        self.widget.setGeometry(QtCore.QRect(40, 20, 151, 97))
        self.widget.setObjectName("widget")
        self.formLayout = QtWidgets.QFormLayout(self.widget)
        self.formLayout.setContentsMargins(0, 0, 0, 0)
        self.formLayout.setObjectName("formLayout")
        self.btnVisible = QtWidgets.QPushButton(self.widget)
        self.btnVisible.setObjectName("btnVisible")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.btnVisible)
        self.editVisible = QtWidgets.QLineEdit(self.widget)
        self.editVisible.setObjectName("editVisible")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.editVisible)
        self.btnEnabled = QtWidgets.QPushButton(self.widget)
        self.btnEnabled.setObjectName("btnEnabled")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.btnEnabled)
        self.editEnabled = QtWidgets.QLineEdit(self.widget)
        self.editEnabled.setObjectName("editEnabled")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.editEnabled)
        self.btnFlat = QtWidgets.QPushButton(self.widget)
        self.btnFlat.setFlat(False)
        self.btnFlat.setObjectName("btnFlat")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.btnFlat)
        self.editFlat = QtWidgets.QLineEdit(self.widget)
        self.editFlat.setObjectName("editFlat")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.editFlat)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 230, 23))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
        MainWindow.setTabOrder(self.editVisible, self.editEnabled)
        MainWindow.setTabOrder(self.editEnabled, self.editFlat)
        MainWindow.setTabOrder(self.editFlat, self.btnVisible)
        MainWindow.setTabOrder(self.btnVisible, self.btnEnabled)
        MainWindow.setTabOrder(self.btnEnabled, self.btnFlat)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Off/On"))
        self.btnVisible.setText(_translate("MainWindow", "Visible"))
        self.editVisible.setText(_translate("MainWindow", "on"))
        self.btnEnabled.setText(_translate("MainWindow", "Enabled"))
        self.editEnabled.setText(_translate("MainWindow", "on"))
        self.btnFlat.setText(_translate("MainWindow", "Flat"))
        self.editFlat.setText(_translate("MainWindow", "on"))


