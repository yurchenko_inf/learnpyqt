import sys
from offOn import *


class MyWin(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.ui.editVisible.returnPressed.connect(self.changeVisible)
        self.ui.editEnabled.returnPressed.connect(self.changeEnabled)
        self.ui.editFlat.returnPressed.connect(self.changeFlat)

    def changeVisible(self):
        if self.ui.editVisible.text() == "off":
            self.ui.btnVisible.setVisible(False)
        elif self.ui.editVisible.text() == "on":
            self.ui.btnVisible.setVisible(True)

    def changeEnabled(self):
        if self.ui.editEnabled.text() == "off":
            self.ui.btnEnabled.setEnabled(False)
        elif self.ui.editEnabled.text() == "on":
            self.ui.btnEnabled.setEnabled(True)

    def changeFlat(self):
        if self.ui.editFlat.text() == "off":
            self.ui.btnFlat.setFlat(False)
        elif self.ui.editFlat.text() == "on":
            self.ui.btnFlat.setFlat(True)


app = QtWidgets.QApplication(sys.argv)
window = MyWin()
window.show()
app.exec_()
