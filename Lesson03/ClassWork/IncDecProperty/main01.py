import sys
from incdec import *
# Решение "в лоб"


class MyWin(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.setStartValue()

        self.ui.dec_x.clicked.connect(self.onDecX)
        self.ui.inc_x.clicked.connect(self.onIncX)

        self.ui.dec_y.clicked.connect(self.onDecY)
        self.ui.inc_y.clicked.connect(self.onIncY)

        self.ui.inc_height.clicked.connect(self.onIncH)
        self.ui.dec_height.clicked.connect(self.onDecH)

        self.ui.dec_width.clicked.connect(self.onDecW)
        self.ui.inc_width.clicked.connect(self.onIncW)

    def setStartValue(self):
        self.ui.value_x.setText(str(self.ui.rect.x()))
        self.ui.value_y.setText(str(self.ui.rect.y()))
        self.ui.value_width.setText(str(self.ui.rect.width()))
        self.ui.value_height.setText(str(self.ui.rect.height()))

    def onDecX(self):
        x = int(self.ui.value_x.text())
        y = int(self.ui.value_y.text())
        x -= 5
        self.ui.value_x.setText(str(x))
        self.ui.rect.move(x, y)

    def onIncX(self):
        x = int(self.ui.value_x.text())
        y = int(self.ui.value_y.text())
        x += 5
        self.ui.value_x.setText(str(x))
        self.ui.rect.move(x, y)

    def onDecY(self):
        x = int(self.ui.value_x.text())
        y = int(self.ui.value_y.text())
        y -= 5
        self.ui.value_y.setText(str(y))
        self.ui.rect.move(x, y)

    def onIncY(self):
        x = int(self.ui.value_x.text())
        y = int(self.ui.value_y.text())
        y += 5
        self.ui.value_y.setText(str(y))
        self.ui.rect.move(x, y)

    def onIncH(self):
        w = int(self.ui.value_width.text())
        h = int(self.ui.value_height.text())
        h += 5
        self.ui.value_height.setText(str(h))
        self.ui.rect.resize(w, h)

    def onDecH(self):
        w = int(self.ui.value_width.text())
        h = int(self.ui.value_height.text())
        h -= 5
        self.ui.value_height.setText(str(h))
        self.ui.rect.resize(w, h)

    def onIncW(self):
        w = int(self.ui.value_width.text())
        h = int(self.ui.value_height.text())
        w += 5
        self.ui.value_width.setText(str(w))
        self.ui.rect.resize(w, h)

    def onDecW(self):
        w = int(self.ui.value_width.text())
        h = int(self.ui.value_height.text())
        w -= 5
        self.ui.value_width.setText(str(w))
        self.ui.rect.resize(w, h)

    # И далее аналогично для ширины...


app = QtWidgets.QApplication(sys.argv)
window = MyWin()
window.show()
app.exec_()