import sys
from blockClicker import *


class MyWin(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.btnClick.clicked.connect(self.onClick)
        self.num_clicks = 10

    def onClick(self):
        self.num_clicks -= 1
        if self.num_clicks == 0:
            self.ui.btnClick.setEnabled(False)
            self.ui.btnClick.setText("Заблокировано")
        else:
            self.ui.btnClick.setText("Кликов до блокировки {}".format(self.num_clicks))

app = QtWidgets.QApplication(sys.argv)
window = MyWin()
window.show()
app.exec_()