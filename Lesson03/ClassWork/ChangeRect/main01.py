import sys
from changeRect import *
# from PyQt5.QtGui import QIntValidator


class MyWin(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.btnSet.clicked.connect(self.onBtnSet)
        # self.ui.edit_x.setValidator(QIntValidator())

    def onBtnSet(self):
        x = int(self.ui.edit_x.text())
        y = int(self.ui.edit_y.text())
        width = int(self.ui.edit_w.text())
        height = int(self.ui.edit_h.text())
        self.ui.rect.setGeometry(x, y, width, height)


app = QtWidgets.QApplication(sys.argv)
window = MyWin()
window.show()
app.exec_()
