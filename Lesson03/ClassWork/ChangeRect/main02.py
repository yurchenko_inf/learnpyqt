import sys
from changeRect import *


class MyWin(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        # self.ui.btnSet.clicked.connect(self.onBtnSet)
        self.ui.btnSet.setVisible(False)

        # Параметры изменяются также при нажатии на кнопку Enter
        self.ui.edit_x.returnPressed.connect(self.onBtnSet)
        self.ui.edit_y.returnPressed.connect(self.onBtnSet)
        self.ui.edit_w.returnPressed.connect(self.onBtnSet)
        self.ui.edit_h.returnPressed.connect(self.onBtnSet)

    def onBtnSet(self):
        x = int(self.ui.edit_x.text())
        y = int(self.ui.edit_y.text())
        width = int(self.ui.edit_w.text())
        height = int(self.ui.edit_h.text())
        # Проверка выхода за границы
        if x + width > self.width():
            x = self.width() - width
        if y + height > self.height():
            y = self.height() - height
        self.ui.rect.setGeometry(x, y, width, height)




app = QtWidgets.QApplication(sys.argv)
window = MyWin()
window.show()
app.exec_()
