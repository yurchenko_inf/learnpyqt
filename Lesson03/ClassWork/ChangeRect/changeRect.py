# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/booblegum/Projects/PyQT/LearnPyQt/Lesson03/ClassWork/ChangeRect/changeRect.ui'
#
# Created by: PyQt5 UI code generator 5.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(826, 282)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.rect = QtWidgets.QWidget(self.centralwidget)
        self.rect.setGeometry(QtCore.QRect(250, 110, 101, 91))
        self.rect.setAcceptDrops(False)
        self.rect.setAutoFillBackground(False)
        self.rect.setStyleSheet("background: black")
        self.rect.setObjectName("rect")
        self.layoutWidget = QtWidgets.QWidget(self.centralwidget)
        self.layoutWidget.setGeometry(QtCore.QRect(10, 10, 142, 161))
        self.layoutWidget.setObjectName("layoutWidget")
        self.formLayout = QtWidgets.QFormLayout(self.layoutWidget)
        self.formLayout.setContentsMargins(0, 0, 0, 0)
        self.formLayout.setObjectName("formLayout")
        self.label = QtWidgets.QLabel(self.layoutWidget)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.edit_x = QtWidgets.QLineEdit(self.layoutWidget)
        self.edit_x.setDragEnabled(True)
        self.edit_x.setObjectName("edit_x")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.edit_x)
        self.label_2 = QtWidgets.QLabel(self.layoutWidget)
        self.label_2.setObjectName("label_2")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_2)
        self.edit_y = QtWidgets.QLineEdit(self.layoutWidget)
        self.edit_y.setObjectName("edit_y")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.edit_y)
        self.label_3 = QtWidgets.QLabel(self.layoutWidget)
        self.label_3.setObjectName("label_3")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_3)
        self.edit_w = QtWidgets.QLineEdit(self.layoutWidget)
        self.edit_w.setObjectName("edit_w")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.edit_w)
        self.label_4 = QtWidgets.QLabel(self.layoutWidget)
        self.label_4.setObjectName("label_4")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.label_4)
        self.edit_h = QtWidgets.QLineEdit(self.layoutWidget)
        self.edit_h.setObjectName("edit_h")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.edit_h)
        self.btnSet = QtWidgets.QPushButton(self.layoutWidget)
        self.btnSet.setEnabled(True)
        self.btnSet.setObjectName("btnSet")
        self.formLayout.setWidget(4, QtWidgets.QFormLayout.FieldRole, self.btnSet)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label.setText(_translate("MainWindow", "x:"))
        self.edit_x.setText(_translate("MainWindow", "220"))
        self.label_2.setText(_translate("MainWindow", "y:"))
        self.edit_y.setText(_translate("MainWindow", "150"))
        self.label_3.setText(_translate("MainWindow", "width:"))
        self.edit_w.setText(_translate("MainWindow", "70"))
        self.label_4.setText(_translate("MainWindow", "height:"))
        self.edit_h.setText(_translate("MainWindow", "80"))
        self.btnSet.setText(_translate("MainWindow", "Задать"))


