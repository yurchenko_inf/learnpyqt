import sys
from design_digger import *
import random

from PyQt5.QtWidgets import QLabel
from PyQt5.QtGui import QPixmap

from PyQt5.QtCore import Qt


class MyWin(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.x = 310
        self.y = 10
        self.ui.digger.move(self.x, self.y)
        self.ui.progressBar_3.setValue(0)
        self.ui.btnLeft.clicked.connect(self.onMoveLeft)
        self.ui.btnRight.clicked.connect(self.onMoveRight)
        self.ui.btnUp.clicked.connect(self.onMoveUp)
        self.ui.btnDown.clicked.connect(self.onMoveDown)
        self.ui.btnDig.clicked.connect(self.onDig)


        treasure_x = random.randint(0, 6)*100
        treasure_y = random.randint(0, 6)*100
        print(treasure_x, treasure_y)
        self.treasure = QLabel(self.ui.centralwidget)
        self.treasure.setScaledContents(True)
        self.treasure.setGeometry(treasure_x+self.x, treasure_y+self.y, 90, 90)
        treasure_pixmap = QPixmap("img/treasure03.png")
        self.treasure.setPixmap(treasure_pixmap)
        self.treasure.setVisible(False)


    def onMoveLeft(self):
        self.x -= 100
        pixmap = QPixmap("img/digger_mole01.png")
        self.ui.digger.setPixmap(pixmap)
        self.ui.digger.move(self.x, self.y)

    def onMoveRight(self):
        self.x += 100
        pixmap = QPixmap("img/digger_mole01_right.png")
        self.ui.digger.setPixmap(pixmap)
        self.ui.digger.move(self.x, self.y)

    def onMoveUp(self):
        self.y -= 100
        self.ui.digger.move(self.x, self.y)

    def onMoveDown(self):
        self.y += 100
        self.ui.digger.move(self.x, self.y)

    def onDig(self):
        lblHole = QLabel(self.ui.centralwidget)
        pixmap = QPixmap('img/pit.png')

        lblHole.setScaledContents(True)
        lblHole.setPixmap(pixmap)
        lblHole.setGeometry(self.x - 10, self.y + 40, 95, 60)

        lblHole.show()

        self.ui.progressBar_3.setValue(self.ui.progressBar_3.value()+10)

        self.ui.digger.raise_()

        if self.ui.digger.x() == self.treasure.x() and self.ui.digger.y() == self.treasure.y():
            print("Treasure")
            self.treasure.setVisible(True)
            self.treasure.raise_()
            box_pixmap = QPixmap("img/treasure.svg")
            self.ui.box1.setPixmap(box_pixmap)

    def keyPressEvent(self, event):
        print(event.key(), event)
        if event.key() == Qt.Key_A:
            self.ui.btnLeft.click()
            # self.onMoveLeft()
        if event.key() == Qt.Key_D:
            self.onMoveRight()
        if event.key() == Qt.Key_W:
            self.onMoveUp()
        if event.key() == Qt.Key_S:
            self.onMoveDown()
        if event.key() == Qt.Key_Q:
            self.onDig()
        # if event.key() == QtCore.Qt.11:
        #     self.close()
        # if event.key() == QtCore.Qt.Key_Space:
        #     global gridLayout
        #     item = gridLayout.itemAtPosition(1, 1)
        #     w = item.widget()
        #     w.setText("test")


app = QtWidgets.QApplication(sys.argv)
window = MyWin()
window.setStyleSheet(open("digger_style.qss", "r").read())
window.show()
app.exec_()
