# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/booblegum/Projects/PyQT/LearnPyQt/Lesson04/ClassWork/AboutStyle/aboutStyle.ui'
#
# Created by: PyQt5 UI code generator 5.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(279, 425)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.widget = QtWidgets.QWidget(self.centralwidget)
        self.widget.setGeometry(QtCore.QRect(10, 10, 261, 401))
        self.widget.setObjectName("widget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.widget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.grData = QtWidgets.QGroupBox(self.widget)
        self.grData.setObjectName("grData")
        self.inpSurName = QtWidgets.QLineEdit(self.grData)
        self.inpSurName.setGeometry(QtCore.QRect(11, 65, 241, 26))
        self.inpSurName.setObjectName("inpSurName")
        self.inpName = QtWidgets.QLineEdit(self.grData)
        self.inpName.setGeometry(QtCore.QRect(11, 30, 241, 26))
        self.inpName.setObjectName("inpName")
        self.inpAge = QtWidgets.QLineEdit(self.grData)
        self.inpAge.setGeometry(QtCore.QRect(11, 100, 241, 26))
        self.inpAge.setObjectName("inpAge")
        self.inpSchool = QtWidgets.QLineEdit(self.grData)
        self.inpSchool.setGeometry(QtCore.QRect(11, 135, 241, 26))
        self.inpSchool.setObjectName("inpSchool")
        self.verticalLayout.addWidget(self.grData)
        self.btnFormat = QtWidgets.QPushButton(self.widget)
        self.btnFormat.setObjectName("btnFormat")
        self.verticalLayout.addWidget(self.btnFormat)
        self.grInfo = QtWidgets.QGroupBox(self.widget)
        self.grInfo.setObjectName("grInfo")
        self.total = QtWidgets.QTextBrowser(self.grInfo)
        self.total.setGeometry(QtCore.QRect(10, 30, 241, 131))
        self.total.setObjectName("total")
        self.verticalLayout.addWidget(self.grInfo)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "About-Style"))
        self.grData.setTitle(_translate("MainWindow", "Заполните анкету"))
        self.inpSurName.setPlaceholderText(_translate("MainWindow", "Фамилия"))
        self.inpName.setPlaceholderText(_translate("MainWindow", "Имя"))
        self.inpAge.setPlaceholderText(_translate("MainWindow", "Возраст"))
        self.inpSchool.setPlaceholderText(_translate("MainWindow", "Школа"))
        self.btnFormat.setText(_translate("MainWindow", "Сформировать информацию"))
        self.grInfo.setTitle(_translate("MainWindow", "Информация обо мне"))


