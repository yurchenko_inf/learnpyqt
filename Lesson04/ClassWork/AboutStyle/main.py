import sys
from aboutStyle import *


class MyWin(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        # Тут будем писать код нашей программы:
        self.ui.btnFormat.clicked.connect(self.onClick)

    def onClick(self):
        total_info = "Привет. Меня зовут {} {}. Мне {} лет и я учусь в {} школе.".format(
            self.ui.inpName.text(),
            self.ui.inpSurName.text(),
            self.ui.inpAge.text(),
            self.ui.inpSchool.text()
        )
        self.ui.total.setText(total_info)


app = QtWidgets.QApplication(sys.argv)
window = MyWin()
window.setStyleSheet(open("style.qss", "r").read())
window.show()
app.exec_()