import sys
from PyQt5.QtWidgets import QApplication, QWidget, QLabel, QFrame
from PyQt5.QtGui import QPixmap

app = QApplication(sys.argv)

widget = QWidget()
widget.resize(500, 500)
widget.move(300, 300)
widget.setWindowTitle('ButterFly')

bf = QLabel(widget)
bf.setGeometry(40, 40, 100, 100)
# pixmap = QPixmap('butterfly_sprite2.png')
# bf.setPixmap(pixmap)
bf.setStyleSheet("""
QLabel{
    border: 1px solid blue;
    background-image: url(butterfly_sprite2.png);
    background-attachment: fixed;
    background-origin: content;

}
""")
widget.show()

sys.exit(app.exec_())
