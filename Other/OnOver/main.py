import sys
from onOver import *


class MyWin(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        # self.setMouseTracking(True)

    # def mouseMoveEvent(self, event):
    #     print("Mouse move", event)

    def eventFilter(self, source, event):
        if event.type() == QtCore.QEvent.MouseMove:
            if event.buttons() == QtCore.Qt.NoButton:
                pos = event.pos()
                print('x: %d, y: %d' % (pos.x(), pos.y()))
            else:
                pass  # do other stuff
        return QtWidgets.QMainWindow.eventFilter(self, source, event)


    def onPress(self):
        self.sender().resize(90, 90)

    def onRelease(self):
        self.sender().resize(self.w, self.h)


app = QtWidgets.QApplication(sys.argv)
window = MyWin()
window.show()
app.installEventFilter(window)
app.exec_()