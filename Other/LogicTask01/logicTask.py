# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/booblegum/Projects/PyQT/LearnPyQt/Lesson03/HomeWork/LogicTask01/logicTask.ui'
#
# Created by: PyQt5 UI code generator 5.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(500, 97)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.widget = QtWidgets.QWidget(self.centralwidget)
        self.widget.setGeometry(QtCore.QRect(10, 10, 476, 73))
        self.widget.setObjectName("widget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.widget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.val01 = QtWidgets.QPushButton(self.widget)
        font = QtGui.QFont()
        font.setPointSize(30)
        self.val01.setFont(font)
        self.val01.setCheckable(False)
        self.val01.setChecked(False)
        self.val01.setFlat(False)
        self.val01.setObjectName("val01")
        self.horizontalLayout.addWidget(self.val01)
        self.label = QtWidgets.QLabel(self.widget)
        font = QtGui.QFont()
        font.setPointSize(32)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        self.val02 = QtWidgets.QPushButton(self.widget)
        font = QtGui.QFont()
        font.setPointSize(30)
        self.val02.setFont(font)
        self.val02.setObjectName("val02")
        self.horizontalLayout.addWidget(self.val02)
        self.label_2 = QtWidgets.QLabel(self.widget)
        font = QtGui.QFont()
        font.setPointSize(36)
        self.label_2.setFont(font)
        self.label_2.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.label_2.setAutoFillBackground(False)
        self.label_2.setFrameShadow(QtWidgets.QFrame.Plain)
        self.label_2.setTextFormat(QtCore.Qt.AutoText)
        self.label_2.setScaledContents(False)
        self.label_2.setAlignment(QtCore.Qt.AlignCenter)
        self.label_2.setObjectName("label_2")
        self.horizontalLayout.addWidget(self.label_2)
        self.val03 = QtWidgets.QPushButton(self.widget)
        font = QtGui.QFont()
        font.setPointSize(30)
        self.val03.setFont(font)
        self.val03.setObjectName("val03")
        self.horizontalLayout.addWidget(self.val03)
        self.label_3 = QtWidgets.QLabel(self.widget)
        font = QtGui.QFont()
        font.setPointSize(32)
        self.label_3.setFont(font)
        self.label_3.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.label_3.setAutoFillBackground(False)
        self.label_3.setFrameShadow(QtWidgets.QFrame.Plain)
        self.label_3.setTextFormat(QtCore.Qt.AutoText)
        self.label_3.setScaledContents(False)
        self.label_3.setAlignment(QtCore.Qt.AlignCenter)
        self.label_3.setObjectName("label_3")
        self.horizontalLayout.addWidget(self.label_3)
        self.result = QtWidgets.QPushButton(self.widget)
        font = QtGui.QFont()
        font.setPointSize(30)
        self.result.setFont(font)
        self.result.setObjectName("result")
        self.horizontalLayout.addWidget(self.result)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.val01.setText(_translate("MainWindow", "5"))
        self.label.setText(_translate("MainWindow", "+"))
        self.val02.setText(_translate("MainWindow", "6"))
        self.label_2.setText(_translate("MainWindow", "-"))
        self.val03.setText(_translate("MainWindow", "2"))
        self.label_3.setText(_translate("MainWindow", "="))
        self.result.setText(_translate("MainWindow", "3"))


