import sys
from logicTask import *


class MyWin(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.first = None

        self.ui.val01.clicked.connect(self.onClick)
        self.ui.val02.clicked.connect(self.onClick)
        self.ui.val03.clicked.connect(self.onClick)
        self.ui.result.clicked.connect(self.onClick)

    def onClick(self):
        if self.first is None:
            self.first = self.sender()
            self.first.setStyleSheet("color:blue")
            return
        self.first.setStyleSheet("color:black")
        text_1 = self.first.text()
        text_2 = self.sender().text()

        text_1, text_2 = text_2, text_1
        self.first.setText(text_1)
        self.sender().setText(text_2)
        self.first = None

        self.checkWin()

    def checkWin(self):
        a = int(self.ui.val01.text())
        b = int(self.ui.val02.text())
        c = int(self.ui.val03.text())
        res = int(self.ui.result.text())
        if a + b - c == res:
            self.ui.widget.setStyleSheet("background-color: green")
        else:
            self.ui.widget.setStyleSheet("background-color: red")


app = QtWidgets.QApplication(sys.argv)
window = MyWin()
window.show()
app.exec_()
