# Источник: https://tproger.ru/translations/python-desktop-notifications/

import notify2
# import rates


def notify(title, text):
    ICON_PATH = "полный путь до иконки"

    # # Получаем текущий курс
    # bitcoin = rates.fetch_bitcoin()

    # Инициализируем d-bus соединение
    notify2.init("Cryptocurrency rates notifier")

    # Создаем Notification-объект
    n = notify2.Notification("Crypto Notifier", icon=ICON_PATH)

    # Устанавливаем уровень срочности
    n.set_urgency(notify2.URGENCY_NORMAL)

    # Устанавливаем задержку
    n.set_timeout(1000)

    result = text

    # Обновляем содержимое
    n.update(title, result)

    # Показываем уведомление
    n.show()

notify("Say hello", "hello")