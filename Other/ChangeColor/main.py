import sys
from changeColor import *


class MyWin(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.btnClick.clicked.connect(self.changeBgColor)

    def changeBgColor(self):
        self.ui.box.setStyleSheet("background-color: red;")


app = QtWidgets.QApplication(sys.argv)
window = MyWin()
window.setStyleSheet(open("style.qss", "r").read())
window.show()
app.exec_()