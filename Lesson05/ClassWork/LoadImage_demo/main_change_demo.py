import sys
from formWithButton import *

from PyQt5.QtWidgets import QLabel
from PyQt5.QtGui import QPixmap


class MyWin(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        # Добавляем self - чтобы можно было обратиться к компоненту из функции-обработчика
        self.label = QLabel(self)
        self.label.setGeometry(50, 50, 256, 256)

        pixmap = QPixmap('smile01.png')
        self.label.setPixmap(pixmap)
        self.label.setScaledContents(True)

        self.ui.btnChange.clicked.connect(self.onChange)

    def onChange(self):
        new_pixmap = QPixmap('smile03_big.png')
        self.label.setPixmap(new_pixmap)

app = QtWidgets.QApplication(sys.argv)
window = MyWin()
window.show()
app.exec_()