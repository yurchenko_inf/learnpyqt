import sys
from emptyForm import *

from PyQt5.QtWidgets import QLabel
from PyQt5.QtGui import QPixmap


class MyWin(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        label = QLabel(self)
        label.setGeometry(50, 50, 256, 256)

        pixmap = QPixmap('smile01.png')
        # TODO: 1) раскомментируйте для демонстрации загрузки изображения из отдельной папки
        # pixmap = QPixmap('img/smile02.png')
        # TODO: 2.1) раскомментируйте для демонстрации загрузки большого изображения
        # pixmap = QPixmap('smile03_big.png')
        label.setPixmap(pixmap)
        # TODO: 2.2) раскомментируйте для демонстрации авто-масштабирования изображения
        # label.setScaledContents(True)

app = QtWidgets.QApplication(sys.argv)
window = MyWin()
window.show()
app.exec_()