import sys
from evolution_steps import *

from PyQt5.QtWidgets import QLabel
from PyQt5.QtGui import QPixmap


class MyWin(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.btnNext.clicked.connect(self.onBtnNext)
        self.step = 1

    def onBtnNext(self):
        self.step += 1
        if self.step == 2:
            pixmap = QPixmap("img/evolution_step2_3")
            self.ui.lblStep2.setPixmap(pixmap)
        elif self.step == 3:
            pixmap = QPixmap("img/evolution_step3_3")
            self.ui.lblStep3.setPixmap(pixmap)
        elif self.step == 4:
            pixmap = QPixmap("img/evolution_step4_3")
            self.ui.lblStep4.setPixmap(pixmap)
        elif self.step == 5:
            pixmap = QPixmap("img/evolution_step5_3")
            self.ui.lblStep5.setPixmap(pixmap)
        elif self.step == 6:
            pixmap = QPixmap("img/evolution_step6_3")
            self.ui.lblStep6.setPixmap(pixmap)
            self.ui.btnNext.setVisible(False)

app = QtWidgets.QApplication(sys.argv)
window = MyWin()
window.show()
app.exec_()