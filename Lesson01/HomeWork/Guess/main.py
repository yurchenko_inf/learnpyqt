import sys
from guess import *


class MyWin(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        # Тут будем писать код нашей программы:
        self.ui.btn1.clicked.connect(self.onClickBtn1)
        self.ui.btn2.clicked.connect(self.onClickBtn2)
        self.ui.btn3.clicked.connect(self.onClickBtn3)
        self.ui.btn4.clicked.connect(self.onClickBtn4)
        self.ui.btn5.clicked.connect(self.onClickBtn5)
        self.ui.btn6.clicked.connect(self.onClickBtn6)
        self.ui.btn7.clicked.connect(self.onClickBtn7)
        self.ui.btn8.clicked.connect(self.onClickBtn8)
        self.ui.btn9.clicked.connect(self.onClickBtn9)

    def onClickBtn1(self):
        print("Увы, мимо -(")

    def onClickBtn2(self):
        print("Увы, мимо -(")

    def onClickBtn3(self):
        print("Поздравляю!")

    def onClickBtn4(self):
        print("Увы, мимо -(")

    def onClickBtn5(self):
        print("Увы, мимо -(")

    def onClickBtn6(self):
        print("Увы, мимо -(")

    def onClickBtn7(self):
        print("Увы, мимо -(")

    def onClickBtn8(self):
        print("Увы, мимо -(")

    def onClickBtn9(self):
        print("Увы, мимо -(")


app = QtWidgets.QApplication(sys.argv)
window = MyWin()
window.show()
app.exec_()
