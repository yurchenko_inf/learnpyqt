import sys

from buttons import *


class MyWin(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        # Тут будем писать код нашей программы:
        self.ui.btn1.clicked.connect(self.onClickBtn1)
        self.ui.btn2.clicked.connect(self.onClickBtn2)
        self.ui.btn3.clicked.connect(self.onClickBtn3)
        self.ui.btn4.clicked.connect(self.onClickBtn4)

    def onClickBtn1(self):
        print("Нажата кнопка 1")

    def onClickBtn2(self):
        print("Нажата кнопка 2")

    def onClickBtn3(self):
        print("Нажата кнопка 3")

    def onClickBtn4(self):
        print("Нажата кнопка 4")


app = QtWidgets.QApplication(sys.argv)
window = MyWin()
window.show()
app.exec_()
