import sys
from auth_form import *  # design - это имя вашего py-файла, получившегося после конвертации


class MyWin(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        # Тут будем писать код нашей программы:
        # ...


app = QtWidgets.QApplication(sys.argv)  # Создаем приложение(программы)
window = MyWin()  # Создаём окно(именно в окне отображаются все виджеты)
window.show()  # Показываем окно
app.exec_()  # и запускаем приложение
