import sys
from calc import *


class MyWin(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        # Тут будем писать код нашей программы:
        # Обработчик все же придется вешать отдельно на каждый виджет-кнопки
        self.ui.btn1.clicked.connect(self.onBtn)
        self.ui.btn2.clicked.connect(self.onBtn)
        self.ui.btn3.clicked.connect(self.onBtn)
        self.ui.btn4.clicked.connect(self.onBtn)
        self.ui.btn5.clicked.connect(self.onBtn)
        self.ui.btn6.clicked.connect(self.onBtn)
        self.ui.btn7.clicked.connect(self.onBtn)
        self.ui.btn8.clicked.connect(self.onBtn)
        self.ui.btn9.clicked.connect(self.onBtn)
        self.ui.btn0.clicked.connect(self.onBtn)
        self.ui.btnPlus.clicked.connect(self.onBtn)
        self.ui.btnMinus.clicked.connect(self.onBtn)
        self.ui.btnDiv.clicked.connect(self.onBtn)
        self.ui.btnMult.clicked.connect(self.onBtn)
        self.ui.btnPoint.clicked.connect(self.onBtn)

        self.ui.btnClear.clicked.connect(self.onBtnClear)

    # Но функцию-обработчик можно сделать общую
    def onBtn(self):
        old_text = self.ui.inpResult.text()
        # Через self.sender() мы получаем доступ к тому виджету, который вызвал функцию
        self.ui.inpResult.setText(old_text + self.sender().text())

    # Функция-обработчик очистки остается без изменений, т.к. она имеет уникальный код
    def onBtnClear(self):
        self.ui.inpResult.setText('')


app = QtWidgets.QApplication(sys.argv)
window = MyWin()
window.show()
app.exec_()