import sys
from calc import *


class MyWin(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        # Тут будем писать код нашей программы:
        self.ui.btn1.clicked.connect(self.onBtn1)
        self.ui.btn2.clicked.connect(self.onBtn2)
        self.ui.btn3.clicked.connect(self.onBtn3)
        self.ui.btn4.clicked.connect(self.onBtn4)
        self.ui.btnPlus.clicked.connect(self.onBtnPlus)
        self.ui.btnMinus.clicked.connect(self.onBtnMinus)
        self.ui.btnClear.clicked.connect(self.onBtnMinus)
        self.ui.btnClear.clicked.connect(self.onBtnClear)

    def onBtn1(self):
        old_text = self.ui.inpResult.text()
        self.ui.inpResult.setText(old_text + '1')

    def onBtn2(self):
        old_text = self.ui.inpResult.text()
        self.ui.inpResult.setText(old_text + '2')

    def onBtn3(self):
        old_text = self.ui.inpResult.text()
        self.ui.inpResult.setText(old_text + '3')

    def onBtn4(self):
        old_text = self.ui.inpResult.text()
        self.ui.inpResult.setText(old_text + '4')

    def onBtnPlus(self):
        old_text = self.ui.inpResult.text()
        self.ui.inpResult.setText(old_text + '+')

    def onBtnMinus(self):
        old_text = self.ui.inpResult.text()
        self.ui.inpResult.setText(old_text + '-')

    # И так по аналогии для всех кнопок...
    # Куча однотипного кода, но в текущем ДЗ это нормально, по другому мы пока не умеем

    def onBtnClear(self):
        self.ui.inpResult.setText('')


app = QtWidgets.QApplication(sys.argv)
window = MyWin()
window.show()
app.exec_()