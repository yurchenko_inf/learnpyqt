import sys

from primitive_calc_v1 import *


class MyWin(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        # Тут будем писать код нашей программы:
        self.ui.btnCalc.clicked.connect(self.onCalc)

    def onCalc(self):
        if self.ui.valA.text() == '' or self.ui.valB.text() == '':
            return
        res = int(self.ui.valA.text()) + int(self.ui.valB.text())
        self.ui.valResult.setText(str(res))


app = QtWidgets.QApplication(sys.argv)
window = MyWin()
window.show()
app.exec_()
