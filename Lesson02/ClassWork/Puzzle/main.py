import sys
from puzzle import *


class MyWin(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        # Тут будем писать код нашей программы:
        self.ui.pushButton.clicked.connect(self.onClick)

    def onClick(self):
        print("Ваши данные проверяются")


app = QtWidgets.QApplication(sys.argv)
window = MyWin()
window.show()
app.exec_()