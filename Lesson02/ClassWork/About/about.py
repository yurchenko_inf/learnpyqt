# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/booblegum/Projects/PyQT/LearnPyQt/Lesson02/ClassWork/About/about.ui'
#
# Created by: PyQt5 UI code generator 5.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(288, 368)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayoutWidget = QtWidgets.QWidget(self.centralwidget)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(10, 10, 271, 351))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(self.verticalLayoutWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setObjectName("formLayout")
        self.label_2 = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.label_2.setObjectName("label_2")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label_2)
        self.inpName = QtWidgets.QLineEdit(self.verticalLayoutWidget)
        self.inpName.setObjectName("inpName")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.inpName)
        self.label_3 = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.label_3.setObjectName("label_3")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_3)
        self.inpSurName = QtWidgets.QLineEdit(self.verticalLayoutWidget)
        self.inpSurName.setObjectName("inpSurName")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.inpSurName)
        self.label_4 = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.label_4.setObjectName("label_4")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_4)
        self.inpAge = QtWidgets.QLineEdit(self.verticalLayoutWidget)
        self.inpAge.setObjectName("inpAge")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.inpAge)
        self.label_5 = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.label_5.setObjectName("label_5")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.label_5)
        self.inpSchool = QtWidgets.QLineEdit(self.verticalLayoutWidget)
        self.inpSchool.setObjectName("inpSchool")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.inpSchool)
        self.verticalLayout.addLayout(self.formLayout)
        self.btnFormat = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.btnFormat.setObjectName("btnFormat")
        self.verticalLayout.addWidget(self.btnFormat)
        self.label_6 = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.label_6.setObjectName("label_6")
        self.verticalLayout.addWidget(self.label_6)
        self.total = QtWidgets.QTextBrowser(self.verticalLayoutWidget)
        self.total.setObjectName("total")
        self.verticalLayout.addWidget(self.total)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "About"))
        self.label.setText(_translate("MainWindow", "Пожалуйста, заполните анкету:"))
        self.label_2.setText(_translate("MainWindow", "Имя"))
        self.label_3.setText(_translate("MainWindow", "Фамилия"))
        self.label_4.setText(_translate("MainWindow", "Возраст"))
        self.label_5.setText(_translate("MainWindow", "Школа"))
        self.btnFormat.setText(_translate("MainWindow", "Отформатировать данные"))
        self.label_6.setText(_translate("MainWindow", "Информация обо мне:"))


