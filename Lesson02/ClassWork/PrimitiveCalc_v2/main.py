import sys

from primitive_calc_v2 import *


class MyWin(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        # Тут будем писать код нашей программы:
        self.ui.btnCalc.clicked.connect(self.onCalc)

    def onCalc(self):
        self.ui.sumRes.setText(str(int(self.ui.sumA.text()) + int(self.ui.sumB.text())))
        self.ui.difRes.setText(str(int(self.ui.difA.text()) - int(self.ui.difB.text())))
        self.ui.multRes.setText(str(int(self.ui.multA.text()) * int(self.ui.multB.text())))
        self.ui.divRes.setText(str(int(self.ui.divA.text()) / int(self.ui.divB.text())))


app = QtWidgets.QApplication(sys.argv)
window = MyWin()
window.show()
app.exec_()
