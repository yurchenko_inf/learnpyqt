# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/booblegum/Projects/PyQT/LearnPyQt/Lesson09/HomeWork/SimpleClock/clock.ui'
#
# Created by: PyQt5 UI code generator 5.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(260, 416)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.watch = QtWidgets.QLabel(self.centralwidget)
        self.watch.setGeometry(QtCore.QRect(0, 10, 251, 381))
        font = QtGui.QFont()
        font.setPointSize(24)
        self.watch.setFont(font)
        self.watch.setText("")
        self.watch.setPixmap(QtGui.QPixmap("img/black_watch.png"))
        self.watch.setScaledContents(True)
        self.watch.setAlignment(QtCore.Qt.AlignCenter)
        self.watch.setObjectName("watch")
        self.time = QtWidgets.QLabel(self.centralwidget)
        self.time.setGeometry(QtCore.QRect(40, 150, 171, 91))
        font = QtGui.QFont()
        font.setPointSize(24)
        self.time.setFont(font)
        self.time.setAlignment(QtCore.Qt.AlignCenter)
        self.time.setObjectName("time")
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.time.setText(_translate("MainWindow", "00:00"))


