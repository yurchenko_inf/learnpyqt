import sys
from clock import *

from PyQt5.QtCore import QTimer
from PyQt5.QtGui import QMovie


class MyWin(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.move(1100, 200)

        self.timer = QTimer()
        self.timer.timeout.connect(self.onTimer)
        self.timer.start(1000)

        self.seconds = 50


    def onTimer(self):
        self.seconds += 1
        m = self.seconds // 60
        s = self.seconds % 60
        self.ui.time.setText("{:0>2}:{:0>2}".format(m, s))



app = QtWidgets.QApplication(sys.argv)
window = MyWin()
window.show()
app.exec_()
