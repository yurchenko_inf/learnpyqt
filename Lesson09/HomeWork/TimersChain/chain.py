# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/booblegum/Projects/PyQT/LearnPyQt/Lesson09/HomeWork/TimersChain/chain.ui'
#
# Created by: PyQt5 UI code generator 5.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(453, 376)
        MainWindow.setStyleSheet("QLabel{\n"
"border: 1px solid black;\n"
"}")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.next1 = QtWidgets.QLabel(self.centralwidget)
        self.next1.setGeometry(QtCore.QRect(110, 20, 70, 70))
        self.next1.setText("")
        self.next1.setPixmap(QtGui.QPixmap("img/next.gif"))
        self.next1.setScaledContents(True)
        self.next1.setObjectName("next1")
        self.lblCount1 = QtWidgets.QLabel(self.centralwidget)
        self.lblCount1.setGeometry(QtCore.QRect(20, 20, 70, 70))
        font = QtGui.QFont()
        font.setPointSize(24)
        self.lblCount1.setFont(font)
        self.lblCount1.setAlignment(QtCore.Qt.AlignCenter)
        self.lblCount1.setObjectName("lblCount1")
        self.lblCount2 = QtWidgets.QLabel(self.centralwidget)
        self.lblCount2.setGeometry(QtCore.QRect(190, 20, 70, 70))
        font = QtGui.QFont()
        font.setPointSize(24)
        self.lblCount2.setFont(font)
        self.lblCount2.setAlignment(QtCore.Qt.AlignCenter)
        self.lblCount2.setObjectName("lblCount2")
        self.next2 = QtWidgets.QLabel(self.centralwidget)
        self.next2.setGeometry(QtCore.QRect(270, 20, 70, 70))
        self.next2.setText("")
        self.next2.setPixmap(QtGui.QPixmap("img/next.gif"))
        self.next2.setScaledContents(True)
        self.next2.setObjectName("next2")
        self.lblCount3 = QtWidgets.QLabel(self.centralwidget)
        self.lblCount3.setGeometry(QtCore.QRect(350, 20, 70, 70))
        font = QtGui.QFont()
        font.setPointSize(24)
        self.lblCount3.setFont(font)
        self.lblCount3.setAlignment(QtCore.Qt.AlignCenter)
        self.lblCount3.setObjectName("lblCount3")
        self.cat = QtWidgets.QLabel(self.centralwidget)
        self.cat.setGeometry(QtCore.QRect(170, 100, 250, 250))
        self.cat.setText("")
        self.cat.setPixmap(QtGui.QPixmap("img/dance_cat.gif"))
        self.cat.setScaledContents(True)
        self.cat.setObjectName("cat")
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.lblCount1.setText(_translate("MainWindow", "15"))
        self.lblCount2.setText(_translate("MainWindow", "10"))
        self.lblCount3.setText(_translate("MainWindow", "5"))


