import sys
from chain import *

from PyQt5.QtCore import QTimer
from PyQt5.QtGui import QMovie


class MyWin(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.move(1100, 200)

        self.timer1 = QTimer()
        self.timer1.timeout.connect(self.onTimer1)
        self.timer1.start(400)

        self.ui.lblCount2.setVisible(False)
        self.ui.lblCount3.setVisible(False)
        self.ui.next1.setVisible(False)
        self.ui.next2.setVisible(False)
        self.ui.cat.setVisible(False)

        movie = QMovie('img/next.gif')
        self.ui.next1.setMovie(movie)
        self.ui.next2.setMovie(movie)
        movie.start()

        cat_movie = QMovie('img/dance_cat.gif')
        self.ui.cat.setMovie(cat_movie)
        cat_movie.start()

    def onTimer1(self):
        time = int(self.ui.lblCount1.text()) - 1
        self.ui.lblCount1.setText(str(time))

        if time == 0:
            self.ui.lblCount2.setVisible(True)
            self.ui.next1.setVisible(True)
            self.timer1.stop()
            self.timer2 = QTimer()
            self.timer2.timeout.connect(self.onTimer2)
            self.timer2.start(500)

    def onTimer2(self):
        time = int(self.ui.lblCount2.text()) - 1
        self.ui.lblCount2.setText(str(time))

        if time == 0:
            self.ui.lblCount3.setVisible(True)
            self.ui.next2.setVisible(True)
            self.timer2.stop()
            self.timer3 = QTimer()
            self.timer3.timeout.connect(self.onTimer3)
            self.timer3.start(500)

    def onTimer3(self):
        time = int(self.ui.lblCount3.text()) - 1
        self.ui.lblCount3.setText(str(time))

        if time == 0:
            self.timer3.stop()
            self.ui.cat.setVisible(True)


app = QtWidgets.QApplication(sys.argv)
window = MyWin()
window.show()
app.exec_()
