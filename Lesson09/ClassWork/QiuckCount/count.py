# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/booblegum/Projects/PyQT/LearnPyQt/Lesson09/ClassWork/QiuckCount/count.ui'
#
# Created by: PyQt5 UI code generator 5.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(637, 156)
        MainWindow.setStyleSheet("QLabel{\n"
"border: 1px solid blue;\n"
"}")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.btnStop = QtWidgets.QPushButton(self.centralwidget)
        self.btnStop.setGeometry(QtCore.QRect(498, 100, 96, 51))
        font = QtGui.QFont()
        font.setPointSize(13)
        font.setBold(False)
        font.setWeight(50)
        font.setStrikeOut(False)
        self.btnStop.setFont(font)
        self.btnStop.setObjectName("btnStop")
        self.widget = QtWidgets.QWidget(self.centralwidget)
        self.widget.setGeometry(QtCore.QRect(10, 20, 581, 71))
        self.widget.setObjectName("widget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.widget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.lblNumber1 = QtWidgets.QLabel(self.widget)
        font = QtGui.QFont()
        font.setPointSize(24)
        self.lblNumber1.setFont(font)
        self.lblNumber1.setAlignment(QtCore.Qt.AlignCenter)
        self.lblNumber1.setObjectName("lblNumber1")
        self.horizontalLayout.addWidget(self.lblNumber1)
        self.lbl1_2 = QtWidgets.QLabel(self.widget)
        font = QtGui.QFont()
        font.setPointSize(24)
        self.lbl1_2.setFont(font)
        self.lbl1_2.setAlignment(QtCore.Qt.AlignCenter)
        self.lbl1_2.setObjectName("lbl1_2")
        self.horizontalLayout.addWidget(self.lbl1_2)
        self.lblNumber2 = QtWidgets.QLabel(self.widget)
        font = QtGui.QFont()
        font.setPointSize(24)
        self.lblNumber2.setFont(font)
        self.lblNumber2.setAlignment(QtCore.Qt.AlignCenter)
        self.lblNumber2.setObjectName("lblNumber2")
        self.horizontalLayout.addWidget(self.lblNumber2)
        self.lbl1_3 = QtWidgets.QLabel(self.widget)
        font = QtGui.QFont()
        font.setPointSize(24)
        self.lbl1_3.setFont(font)
        self.lbl1_3.setAlignment(QtCore.Qt.AlignCenter)
        self.lbl1_3.setObjectName("lbl1_3")
        self.horizontalLayout.addWidget(self.lbl1_3)
        self.lblNumber3 = QtWidgets.QLabel(self.widget)
        font = QtGui.QFont()
        font.setPointSize(24)
        self.lblNumber3.setFont(font)
        self.lblNumber3.setAlignment(QtCore.Qt.AlignCenter)
        self.lblNumber3.setObjectName("lblNumber3")
        self.horizontalLayout.addWidget(self.lblNumber3)
        self.lbl1_4 = QtWidgets.QLabel(self.widget)
        font = QtGui.QFont()
        font.setPointSize(24)
        self.lbl1_4.setFont(font)
        self.lbl1_4.setAlignment(QtCore.Qt.AlignCenter)
        self.lbl1_4.setObjectName("lbl1_4")
        self.horizontalLayout.addWidget(self.lbl1_4)
        self.lblResult = QtWidgets.QLabel(self.widget)
        font = QtGui.QFont()
        font.setPointSize(24)
        self.lblResult.setFont(font)
        self.lblResult.setAlignment(QtCore.Qt.AlignCenter)
        self.lblResult.setObjectName("lblResult")
        self.horizontalLayout.addWidget(self.lblResult)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.btnStop.setText(_translate("MainWindow", "STOP!"))
        self.lblNumber1.setText(_translate("MainWindow", "0"))
        self.lbl1_2.setText(_translate("MainWindow", "+"))
        self.lblNumber2.setText(_translate("MainWindow", "0"))
        self.lbl1_3.setText(_translate("MainWindow", "-"))
        self.lblNumber3.setText(_translate("MainWindow", "0"))
        self.lbl1_4.setText(_translate("MainWindow", "="))
        self.lblResult.setText(_translate("MainWindow", "0"))


