import sys
import random
from count import *

from PyQt5.QtCore import QTimer


class MyWin(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.timer1 = QTimer()
        self.timer1.timeout.connect(self.onTimer1)
        self.timer1.start(2400)

        self.timer2 = QTimer()
        self.timer2.timeout.connect(self.onTimer2)
        self.timer2.start(1600)

        self.timer3 = QTimer()
        self.timer3.timeout.connect(self.onTimer3)
        self.timer3.start(800)

        self.timer4 = QTimer()
        self.timer4.timeout.connect(self.onTimer4)
        self.timer4.start(3600)

        self.ui.btnStop.clicked.connect(self.onStop)

    def onTimer1(self):
        self.ui.lblNumber1.setText(str(random.randint(0, 9)))

    def onTimer2(self):
        self.ui.lblNumber2.setText(str(random.randint(0, 9)))

    def onTimer3(self):
        self.ui.lblNumber3.setText(str(random.randint(0, 9)))

    def onTimer4(self):
        self.ui.lblResult.setText(str(random.randint(0, 9)))

    def onStop(self):
        if int(self.ui.lblNumber1.text()) + int(self.ui.lblNumber2.text()) - int(self.ui.lblNumber3.text()) == int(
            self.ui.lblResult.text()):
            self.ui.widget.setStyleSheet("background-color: green;")
            self.timer1.stop()
            self.timer2.stop()
            self.timer3.stop()
            self.timer4.stop()
        else:
            self.ui.widget.setStyleSheet("background-color: red;")


app = QtWidgets.QApplication(sys.argv)
window = MyWin()
window.show()
app.exec_()
