import sys
from rocket import *
from PyQt5.QtCore import QTimer
from PyQt5.QtGui import QMovie

# Статусы
WAIT = 0  # Ожидание
LAUNCH = 1  # Запуск
LAUNCHED = 2  # Запущена


class MyWin(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.ui.btnLaunch.clicked.connect(self.onLaunch)

        self.timer = QTimer()
        self.timer.timeout.connect(self.onTick)

        movie = QMovie('img/rocket_pink.gif')
        self.ui.rocket.setMovie(movie)
        movie.jumpToFrame(0)
        self.state = WAIT
        self.statesText = ["Ожидание", "Запуск", "Запущена"]

    def onLaunch(self):
        if self.state == WAIT:
            self.timer.start(300)
            self.state = LAUNCH
            self.ui.lblState.setText(self.statesText[self.state])

    def onTick(self):
        self.ui.lblTime.setText(str(int(self.ui.lblTime.text()) - 1))

        if self.ui.lblTime.text() == '0':
            self.timer.stop()
            self.state = LAUNCHED
            self.ui.lblState.setText(self.statesText[self.state])
            self.launch()

    def launch(self):
        self.ui.rocket.movie().start()

        self.rocketTimer = QTimer()
        self.rocketTimer.timeout.connect(self.onRocketTimer)
        self.rocketTimer.start(100)

    def onRocketTimer(self):
        self.ui.rocket.move(self.ui.rocket.x(), self.ui.rocket.y() - 5)


app = QtWidgets.QApplication(sys.argv)
window = MyWin()
# window.setStyleSheet(open("buttonStyle.qss", "r").read())
window.show()
app.exec_()
