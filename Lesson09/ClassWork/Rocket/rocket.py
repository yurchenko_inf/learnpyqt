# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/booblegum/Projects/PyQT/LearnPyQt/Lesson09/ClassWork/Rocket/rocket.ui'
#
# Created by: PyQt5 UI code generator 5.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(407, 258)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.rocket = QtWidgets.QLabel(self.centralwidget)
        self.rocket.setGeometry(QtCore.QRect(40, 60, 151, 171))
        self.rocket.setText("")
        self.rocket.setPixmap(QtGui.QPixmap("../Launch/img/rocket_pink.gif"))
        self.rocket.setScaledContents(True)
        self.rocket.setObjectName("rocket")
        self.btnLaunch = QtWidgets.QPushButton(self.centralwidget)
        self.btnLaunch.setGeometry(QtCore.QRect(240, 190, 131, 51))
        font = QtGui.QFont()
        font.setPointSize(15)
        self.btnLaunch.setFont(font)
        self.btnLaunch.setObjectName("btnLaunch")
        self.widget = QtWidgets.QWidget(self.centralwidget)
        self.widget.setGeometry(QtCore.QRect(190, 20, 186, 151))
        self.widget.setObjectName("widget")
        self.gridLayout = QtWidgets.QGridLayout(self.widget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.label_3 = QtWidgets.QLabel(self.widget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_3.setFont(font)
        self.label_3.setObjectName("label_3")
        self.gridLayout.addWidget(self.label_3, 0, 0, 1, 3)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem, 1, 0, 1, 1)
        self.lblTime = QtWidgets.QLabel(self.widget)
        font = QtGui.QFont()
        font.setPointSize(24)
        self.lblTime.setFont(font)
        self.lblTime.setObjectName("lblTime")
        self.gridLayout.addWidget(self.lblTime, 1, 1, 1, 2)
        self.label_4 = QtWidgets.QLabel(self.widget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_4.setFont(font)
        self.label_4.setObjectName("label_4")
        self.gridLayout.addWidget(self.label_4, 2, 0, 1, 1)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem1, 2, 1, 1, 1)
        self.lblState = QtWidgets.QLabel(self.widget)
        font = QtGui.QFont()
        font.setPointSize(18)
        self.lblState.setFont(font)
        self.lblState.setObjectName("lblState")
        self.gridLayout.addWidget(self.lblState, 3, 0, 1, 3)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.btnLaunch.setText(_translate("MainWindow", "Запуск"))
        self.label_3.setText(_translate("MainWindow", "Обратный отсчет"))
        self.lblTime.setText(_translate("MainWindow", "10"))
        self.label_4.setText(_translate("MainWindow", "Статус"))
        self.lblState.setText(_translate("MainWindow", "Ожидает"))


