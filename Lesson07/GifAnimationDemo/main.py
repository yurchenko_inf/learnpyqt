import sys
from demoAnimation import *
from PyQt5.QtGui import QMovie


class MyWin(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        # Загружаем gif'ки для трех виджетов QLabel созданных в дизайнере
        self.movie1 = QMovie('img/mushroom04.gif')
        self.ui.gif01.setMovie(self.movie1)
        self.ui.gif01.setScaledContents(True)
        self.movie1.jumpToFrame(0)

        self.movie2 = QMovie('img/mushroom02.gif')
        self.ui.gif02.setMovie(self.movie2)
        self.ui.gif02.setScaledContents(True)
        self.movie2.jumpToFrame(0)

        self.movie3 = QMovie('img/mushroom03.gif')
        self.ui.gif03.setMovie(self.movie3)
        self.ui.gif03.setScaledContents(True)
        self.movie3.jumpToFrame(0)

        # Добавляем обработчики-событий на все кнопки
        self.ui.btnPlay1.clicked.connect(self.play1)
        self.ui.btnPlay2.clicked.connect(self.play2)
        self.ui.btnPlay3.clicked.connect(self.play3)

        self.ui.btnPause1.clicked.connect(self.pause1)
        self.ui.btnPause2.clicked.connect(self.pause2)
        self.ui.btnPause3.clicked.connect(self.pause3)

        self.ui.btnStop1.clicked.connect(self.stop1)
        self.ui.btnStop2.clicked.connect(self.stop2)
        self.ui.btnStop3.clicked.connect(self.stop3)

    # Создаем функции-обработчики
    def play1(self):
        self.movie1.start()

    def play2(self):
        self.movie2.start()

    def play3(self):
        self.movie3.start()

    def pause1(self):
        self.movie1.stop()

    def pause2(self):
        print(self.movie2.currentFrameNumber())
        print(self.movie2.frameCount())
        self.movie2.stop()

    def pause3(self):
        self.movie3.stop()

    def stop1(self):
        self.movie1.jumpToFrame(0)
        self.movie1.stop()

    def stop2(self):
        self.movie2.jumpToFrame(0)
        self.movie2.stop()

    def stop3(self):
        self.movie3.jumpToFrame(0)
        self.movie3.stop()

app = QtWidgets.QApplication(sys.argv)
window = MyWin()
window.show()
app.exec_()