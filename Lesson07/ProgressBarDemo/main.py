import sys
from progressBar import *


class MyWin(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        # Устанавливаем значения прогресс-баров в 0
        self.ui.progressBar.setValue(0)
        self.ui.progressBarStyle.setValue(0)

        self.ui.slider.valueChanged.connect(self.onSliderChange)

    def onSliderChange(self):
        # 1) Демонстрируем чтение значений из слайдера
        # print(self.ui.slider.value())
        # 2) Демонстрируем задание значений из слайдера прогрессБару
        # self.ui.progressBar.setValue(self.ui.slider.value())
        # 3) Немного математики, чтобы выразить значение в %
        # print((self.ui.slider.value()*100/3))
        self.ui.progressBar.setValue(self.ui.slider.value()*100/3)
        self.ui.progressBarStyle.setValue(self.ui.slider.value()*100/3)

app = QtWidgets.QApplication(sys.argv)
window = MyWin()
window.setStyleSheet(open("style.qss", "r").read())
window.show()
app.exec_()