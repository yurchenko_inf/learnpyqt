# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/booblegum/Projects/PyQT/LearnPyQt/Lesson08/ClassWork/CustomButton/customButton.ui'
#
# Created by: PyQt5 UI code generator 5.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(167, 147)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.btnLeft = QtWidgets.QPushButton(self.centralwidget)
        self.btnLeft.setGeometry(QtCore.QRect(20, 10, 120, 120))
        self.btnLeft.setIconSize(QtCore.QSize(120, 120))
        self.btnLeft.setFlat(True)
        self.btnLeft.setObjectName("btnLeft")
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.btnLeft.setText(_translate("MainWindow", "Click!"))


