import sys
from customButton import *


class MyWin(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)


app = QtWidgets.QApplication(sys.argv)
window = MyWin()
window.setStyleSheet(open("buttonStyle.qss", "r").read())
window.show()
app.exec_()
