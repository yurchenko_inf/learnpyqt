import sys
from pomodoroDesign import *

from PyQt5.QtGui import QMovie, QPixmap, QIcon
from PyQt5.QtCore import Qt, QTime
# import notify2


# def notify(title, text):
#     ICON_PATH = "полный путь до иконки"
#
#     # # Получаем текущий курс
#     # bitcoin = rates.fetch_bitcoin()
#
#     # Инициализируем d-bus соединение
#     notify2.init("Cryptocurrency rates notifier")
#
#     # Создаем Notification-объект
#     n = notify2.Notification("Crypto Notifier", icon=ICON_PATH)
#
#     # Устанавливаем уровень срочности
#     n.set_urgency(notify2.URGENCY_NORMAL)
#
#     # Устанавливаем задержку
#     n.set_timeout(1000)
#
#     result = text
#
#     # Обновляем содержимое
#     n.update(title, result)
#
#     # Показываем уведомление
#     n.show()


class MyWin(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.movie = QMovie('img/timeCircle02.gif')
        self.ui.lblTimeCircle.setMovie(self.movie)
        self.ui.lblTimeCircle.setScaledContents(True)

        self.setWindowFlag(Qt.FramelessWindowHint)

        self.ui.tmp.setVisible(False)

        self.ui.btnClose.clicked.connect(self.close)
        self.ui.btnPlay.clicked.connect(self.onBtnPlay)
        self.ui.btnStop.clicked.connect(self.onBtnStop)

        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.on_timer)
        self.timer.start(100)

        self.seconds = 0
        self.status = 'работа'
        self.play = True

        self.ui.lblStatus.setText(self.status)
        self.showTime()

        self.ui.editWork.setText('2')
        self.ui.editRest.setText('1')

    def onBtnPlay(self):
        if self.play:
            # set pause
            icon = QIcon('img/pomodoro/pause.png')
            self.timer.stop()
        else:
            # set play
            icon = QIcon('img/pomodoro/play.png')
            self.timer.start()
        self.ui.btnPlay.setIcon(icon)
        self.play = not self.play

    def onBtnStop(self):
        self.seconds = 0
        self.showTime()

    def setWindowForm(self):
        pixmap = QPixmap('img/bg_red.png')

        pal = self.palette()
        pal.setBrush(QtGui.QPalette.Normal, QtGui.QPalette.Window,
                     QtGui.QBrush(pixmap))
        pal.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Window,
                     QtGui.QBrush(pixmap))
        self.setPalette(pal)
        self.setMask(pixmap.mask())

    def showTime(self):
        m = self.seconds // 60
        s = self.seconds % 60
        self.ui.lblTime.setText("{:0>2}:{:0>2}".format(m, s))

        self.showCircleTime()

    def showCircleTime(self):
        doTime = 0
        if self.status == 'работа':
            doTime = int(self.ui.editWork.text()) * 60
        elif self.status == 'отдых':
            doTime = int(self.ui.editRest.text()) * 60

        percent_time = (self.seconds / doTime) * 100
        # print(1, int(100 / self.movie.frameCount()))
        # print(2, int(percent_time))
        frame = int(percent_time) // int(100 / (self.movie.frameCount() - 1))
        # print('frame', frame)
        self.movie.jumpToFrame(frame)

    def changeStatus(self):
        if self.status == 'работа':
            self.status = 'отдых'
            # notify("Помодоро Таймер сообщает", "Пора отдохнуть")
            pixmap = QPixmap('img/bg_green.png')
        else:
            self.status = 'работа'
            # notify("Помодоро Таймер сообщает", "Пришло время продолжать работу")
            pixmap = QPixmap('img/bg_red.png')

        pal = self.palette()
        pal.setBrush(QtGui.QPalette.Normal, QtGui.QPalette.Window,
                     QtGui.QBrush(pixmap))
        self.setPalette(pal)
        self.seconds = 0
        self.showTime()

    def on_timer(self):
        self.seconds += 1
        self.showTime()

        if self.status == 'работа':
            doTime = int(self.ui.editWork.text()) * 60
        elif self.status == 'отдых':
            doTime = int(self.ui.editRest.text()) * 60

        if self.seconds >= doTime:
            self.changeStatus()

    def mouseMoveEvent(self, event):
        if event.buttons() == Qt.LeftButton:
            x = event.globalX() - self.mpos.x()
            y = event.globalY() - self.mpos.y()
            self.move(x, y)

    def mousePressEvent(self, event):
        self.mpos = event.pos()

    def close(self):
        app.exit(0)


app = QtWidgets.QApplication(sys.argv)
window = MyWin()
window.setStyleSheet(open("style.qss", "r").read())
window.show()
window.setWindowForm()
app.exec_()
