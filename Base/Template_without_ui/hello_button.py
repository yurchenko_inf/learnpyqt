import sys
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton


def onBtnClick():
    print("Hello")


app = QApplication(sys.argv)

widget = QWidget()
widget.resize(250, 150)
widget.move(300, 300)
widget.setWindowTitle('Hello World')

button = QPushButton("Say Hello!", widget)
button.move(50, 30)
button.clicked.connect(onBtnClick)

widget.show()

sys.exit(app.exec_())


# QObject.setProperty('PropertyName', value)
